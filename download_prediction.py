import pandas as pd
import numpy as np
import urllib
import os 
import glob
import csv

#downloading all images from links
df=pd.read_csv('/home/spartan/Desktop/exportSrilanka.csv') #directore of input file
a=df['attImage']
i=0
while(i<len(a)):
    urllib.urlretrieve(a[i], "/home/spartan/Desktop/allimages/%s.jpg"% i)
    i=i+1



#running model on all images and storing result in names.csv
image_path="/home/spartan/Desktop/allimages" #directory of downloaded images
filenames=glob.glob(image_path+"/*.jpg")
s=[]
with open('names.csv','w') as csvfile:   #result will be written in this file
          
     fieldnames=['name','prediction']
     writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
     writer.writeheader()
     for f in filenames:
         writer.writerow({'name':'%s'% f,'prediction': (str(os.popen('python classify_image.py --model_dir='location of directory in which model is installed' --image %s'% f).readlines()).split('(',1)[0].strip("['"))})

#result.csv file having input csv file with predictions column attached    
df1=pd.read_csv('names.csv')
df['predictions']=df1['prediction']
df.to_csv('result.csv')